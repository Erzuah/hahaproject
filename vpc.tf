data "aws_availability_zones" "available" {}

resource "aws_vpc" "my_vpc" {
  cidr_block           = var.vpc_cidrblock
  enable_dns_hostnames = true
  enable_dns_support   = true
  instance_tenancy     = var.instanceTenancy 

  tags = {
    Name = "myvpc"
  }
}

resource "aws_internet_gateway" "my_internet_gateway" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name = "my_igw"
  }
}

resource "aws_route_table" "my_rt" {
  vpc_id = aws_vpc.my_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_internet_gateway.id
  }

  tags = {
    Name = "myRT"
  }
}

resource "aws_default_route_table" "my_private_rt" {
  default_route_table_id  = aws_vpc.my_vpc.default_route_table_id

  tags = {
    Name = "my_private"
  }
}

resource "aws_subnet" "my_subnet" {
  vpc_id                  = aws_vpc.my_vpc.id
  cidr_block              = var.cidr_block
  availability_zone       = "us-east-2a"

  tags = {
    Name = "public_subnet"
  }
}

resource "aws_route_table_association" "my_assoc" {
  subnet_id      = aws_subnet.my_subnet.id
  route_table_id = aws_route_table.my_rt.id
}